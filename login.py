#!/usr/bin/python
from config import config
from time import sleep
from random import randint
from bs4 import BeautifulSoup

class Login(object):
    """
        """
    def __init__(self, username, password):
        """
        """
        self.site_url = config['app']['url']
        self.username = username
        self.password = password
        self.rest = randint(4, 10)

    def login(self, driver):
        """

        :return:
        """
        logged_in = False
        driver.get(self.site_url)

        # accept cookies
        accept_cookie_btn = driver.find_element_by_xpath('/html/body/div[1]/div/div[5]/button')
        accept_cookie_btn.click()

        nav_login_btn = driver.find_element_by_xpath('//*[@id="top-bar"]/div/nav[2]/div/button[1]')
        nav_login_btn.click()
        sleep(self.rest)

        input_username = driver.find_element_by_id("username")
        input_username.send_keys(self.username)
        sleep(self.rest)

        input_password = driver.find_element_by_id("password")
        input_password.send_keys(self.password)
        sleep(self.rest)

        login_btn = driver.find_element_by_xpath('/html/body/div[1]/div/div[1]/div[2]/span/form/div[3]/button/span')
        login_btn.click()
        sleep(self.rest)

        source_code = driver.page_source
        soup = BeautifulSoup(source_code)
        top_nav = soup.find('section', id='top-bar')
        if 'My Bets' in top_nav.text:
            logged_in = True

        return driver, logged_in
