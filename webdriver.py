#!/usr/bin/python
from selenium import webdriver
from selenium.webdriver.chrome.options import Options

import os


class CreateWebDriver(object):
    """
        """
    def __init__(self):
        """
        """
        self.chrome_options = Options()
        self.chrome_options.add_argument("--window-size=1920x1080")
        self.chrome_driver = os.getcwd() + "/chromedriver"

    def get_webdriver(self):
        """

        :return:
        """
        driver = webdriver.Chrome(chrome_options=self.chrome_options, executable_path=self.chrome_driver)
        return driver
