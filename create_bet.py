#!/usr/bin/python
from time import sleep
from selenium.webdriver.support.ui import Select
from random import randint, choice


class CreateBet(object):
    """
    """
    def __init__(self, driver):
        """
        """
        self.driver = driver
        self.rest = randint(3, 6)
        self.horse_racing = False

    def find_dropdown(self, select_type):
        """

        :param select_type: str
        :return:
        """
        select = Select(self.driver.find_element_by_id(select_type))
        return select

    def get_options(self, select_type):
        """

        :param select_type: str
        :return:
        """
        selections = self.find_dropdown(select_type)
        return selections

    def populate_dropdown(self, select_type, in_span=False):
        """

        :param select_type: str
        :param in_span: bool
        :return:
        """
        select_type_options = self.get_options(select_type)
        option_choice = self.select_option(select_type_options.options, in_span)
        print("choice: %s" % option_choice)
        if option_choice.strip() == 'Horse Racing':
            self.horse_racing = True
        if option_choice.strip() == "No Regions Available":
            return None
        select_type_options.select_by_visible_text(option_choice)

        return option_choice

    @staticmethod
    def select_option(options, in_span=False):
        """

        :param options: list
        :param in_span: bool
        :return: str
        """
        option_choice = choice(options)
        if in_span:
            x = option_choice.get_attribute('innerHTML')
            option_text = x.split('<span>')[1].split('</span>')[0]
        else:
            option_text = option_choice.text

        return option_text

    def create_bet(self, stake=10):
        """

        :return:
        """
        bet_placed = False
        bet = {}

        # select sport
        sport = self.populate_dropdown("sport")
        if sport is None:
            return self.driver, bet_placed, bet
        sleep(self.rest)
        bet['sport'] = sport

        # select country
        country = self.populate_dropdown("country")
        if country is None:
            return self.driver, bet_placed, bet
        sleep(self.rest)
        bet['country'] = country

        # select league
        league = self.populate_dropdown("league")
        if league is None:
            return self.driver, bet_placed, bet
        sleep(self.rest)
        bet['league'] = league

        # select game
        game = self.populate_dropdown("game", True)
        if game is None:
            return self.driver, bet_placed, bet
        sleep(self.rest)
        bet['game'] = game

        # select market
        market = self.populate_dropdown("market")
        if market is None:
            return self.driver, bet_placed, bet
        sleep(self.rest)
        bet['market'] = market

        # select selection
        selection = self.populate_dropdown("selection")
        if selection is None:
            return self.driver, bet_placed, bet
        sleep(self.rest)
        bet['selection'] = selection

        # check prices available
        try:
            self.driver.find_element_by_xpath("//*[contains(text(), ' No Odds Available ')]")
            prices_available = False
        except:
            prices_available = True

        if not prices_available:
            return self.driver, bet_placed, bet

        # if horse racing select a bet type
        if self.horse_racing:
            bet_type = self.populate_dropdown("selectedBetType")
            if bet_type is None:
                return self.driver, bet_placed, bet
            sleep(self.rest)
            bet['bet_type'] = bet_type

        if self.horse_racing:
            top_price = self.driver.find_element_by_xpath('//*[@id="main"]/div[3]/div[1]/div/div[7]/div/div/div[2]/div[1]/label/span[1]')
        else:
            top_price = self.driver.find_element_by_xpath('//*[@id="main"]/div[3]/div[1]/div/div[6]/div/div/div[2]/div[1]/label/span[1]')

        #TODO add top price value to bet dict
        bet['price'] = ""
        top_price.click()
        sleep(self.rest)

        input_stake = self.driver.find_element_by_id("stake")
        input_stake.clear()
        input_stake.send_keys(stake)
        sleep(self.rest)
        bet['stake'] = stake

        # check if user has sub accounts available
        try:
            sub_account = self.driver.find_element_by_id("subAccount")
        except selenium.common.exceptions.NoSuchElementException:
            sub_account = False

        if sub_account:
            sub_account_name = self.populate_dropdown("subAccount")
            if sub_account_name is None:
                return self.driver, bet_placed, bet
            bet['sub_account'] = sub_account_name
        sleep(self.rest)

        if sub_account and self.horse_racing:
            place_bet_btn = self.driver.find_element_by_xpath('//*[@id="main"]/div[3]/div[1]/div/div[10]/button')
        elif sub_account:
            place_bet_btn = self.driver.find_element_by_xpath('//*[@id="main"]/div[3]/div[1]/div/div[9]/button')
        else:
            place_bet_btn = self.driver.find_element_by_xpath('//*[@id="main"]/div[3]/div[1]/div/div[8]/button')
        # TODO check bet has been placed.
        place_bet_btn.click()
        sleep(self.rest)

        return self.driver, bet_placed, bet





        # try:
        #     self.driver.find_element_by_xpath("//*[contains(text(), 'There are unfilled fields')]")
        #     bet_placed = False
        # except:
        #     bet_placed = True
        #
        # try:
        #     self.driver.find_element_by_xpath("//*[contains(text(), 'Insufficient funds to place this bet')]")
        #     bet_placed = False
        # except:
        #     bet_placed = True




