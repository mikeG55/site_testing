#!/usr/bin/python
from config import config
from time import sleep
from random import randint
from bs4 import BeautifulSoup


class Register(object):
    """
        """
    def __init__(self, first_name, last_name, email_address, confirm_email_address, accept_terms, username, password,
                 confirm_password, date_of_birth, postcode, contact_number, deposit_limit_time,
                 deposit_limit_amount=None, accept_marketing=False):
        """
        """
        self.site_url = config['app']['url']
        self.first_name = first_name
        self.last_name = last_name
        self.email_address = email_address
        self.confirm_email_address = confirm_email_address
        self.accept_terms = accept_terms
        self.accept_marketing = accept_marketing
        self.username = username
        self.password = password
        self.confirm_password = confirm_password
        self.date_of_birth = date_of_birth
        self.postcode = postcode
        self.contact_number = contact_number
        self.deposit_limit_time = deposit_limit_time
        self.deposit_limit_amount = deposit_limit_amount
        self.rest = randint(4, 10)

    def register(self, driver):
        """

        :return:
        """
        driver.get(self.site_url)

        nav_register_btn = driver.find_element_by_xpath('')
        nav_register_btn.click()
        sleep(self.rest)

        input_first_name = driver.find_element_by_id("first_name")
        input_first_name.send_keys(self.first_name)
        sleep(self.rest)

        input_last_name = driver.find_element_by_id("last_name")
        input_last_name.send_keys(self.last_name)
        sleep(self.rest)

        input_email_address = driver.find_element_by_id("email_address")
        input_email_address.send_keys(self.email_address)
        sleep(self.rest)

        input_confirm_email_address = driver.find_element_by_id("confirm_email_address")
        input_confirm_email_address.send_keys(self.confirm_email_address)
        sleep(self.rest)

        # TODO radio buttons on accept terms and agree marketing

        continue_btn = driver.find_element_by_xpath('')
        continue_btn.click()
        sleep(self.rest)

        input_username = driver.find_element_by_id("username")
        input_username.send_keys(self.username)
        sleep(self.rest)

        input_password = driver.find_element_by_id("password")
        input_password.send_keys(self.password)
        sleep(self.rest)

        input_confirm_password = driver.find_element_by_id("confirm-password")
        input_confirm_password.send_keys(self.confirm_password)
        sleep(self.rest)

        # TODO date of birth

        continue_btn = driver.find_element_by_xpath('')
        continue_btn.click()
        sleep(self.rest)

        input_postcode = driver.find_element_by_id("postcode")
        input_postcode.send_keys(self.postcode)
        sleep(self.rest)

        # TODO click address

        input_contact_number = driver.find_element_by_id("contact_number")
        input_contact_number.send_keys(self.contact_number)
        sleep(self.rest)

        if self.deposit_limit_time != "No Limit":
            input_deposit_limit_amount = driver.find_element_by_id("deposit_limit_amount")
            input_deposit_limit_amount.send_keys(self.deposit_limit_amount)
            sleep(self.rest)

        create_btn = driver.find_element_by_xpath('')
        create_btn.click()
        sleep(self.rest)

        source_code = driver.page_source
        soup = BeautifulSoup(source_code)


