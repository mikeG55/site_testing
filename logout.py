#!/usr/bin/python
from time import sleep
from random import randint
from bs4 import BeautifulSoup

class Logout(object):
    """
        """
    def __init__(self):
        """
        """
        self.rest = randint(4, 10)

    def logout(self, driver):
        """

        :return:
        """
        logged_out = False

        # check logged in
        source_code = driver.page_source
        soup = BeautifulSoup(source_code)
        top_nav = soup.find('section', id='top-bar')
        if 'My Account' not in top_nav.text:
            return driver, logged_out

        # click my account
        nav_my_account_btn = driver.find_element_by_xpath('//*[@id="top-bar"]/div/nav[1]/a[2]')
        nav_my_account_btn.click()
        sleep(self.rest)

        # click log out
        side_nav_logout_btn = driver.find_element_by_xpath('//*[@id="main"]/div[1]/div[4]/button')
        side_nav_logout_btn.click()
        sleep(self.rest)

        # check logged out
        source_code = driver.page_source
        soup = BeautifulSoup(source_code)
        top_nav = soup.find('section', id='top-bar')
        print(top_nav.text)
        if 'My Bets' not in top_nav.text:
            logged_out = True

        return driver, logged_out
