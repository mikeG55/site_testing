#!/usr/bin/python
from config import config
from time import sleep
from random import randint
from webdriver import CreateWebDriver
from login import Login
from create_bet import CreateBet
from logout import Logout


class RunTests(object):
    """
        """

    def __init__(self):
        """
        """
        self.site_url = config['app']['url']
        self.username = config['app']['username']
        self.password = config['app']['password']
        self.rest = randint(4, 10)


    def run_test(self):
        """

        :return:
        """
        wd = CreateWebDriver()
        driver = wd.get_webdriver()
        sleep(self.rest)

        login_obj = Login(self.username, self.password)
        driver, is_logged_in = login_obj.login(driver)
        if not is_logged_in:
            print("Failed to login")
            return

        print("is_logged_in: %s" % is_logged_in)
        sleep(self.rest)

        create_bet_obj = CreateBet(driver)
        bet_placed = False
        while not bet_placed:
            driver, bet_placed = create_bet_obj.create_bet()
            print("bet_placed: %s" % bet_placed)
            sleep(self.rest)

        logout_obj = Logout()
        driver, is_logged_out = logout_obj.logout(driver)
        if not is_logged_out:
            print("Failed to logout")
            return

        print("is_logged_out: %s" % is_logged_out)
        sleep(self.rest)

        driver.quit()


def main():
    l = RunTests()
    l.run_test()


if __name__ == "__main__":
    main()





