#!/usr/bin/python
from time import sleep
from random import randint
from bs4 import BeautifulSoup


class MyBets(object):
    """
    """
    def __init__(self, driver):
        """
        """
        self.driver = driver
        self.rest = randint(3, 6)

    def my_bet(self, driver, settled=False):
        """

        :param driver: object
        :param settled: bool
        :return:
        """
        my_bets_available = False

        # check logged in
        source_code = driver.page_source
        soup = BeautifulSoup(source_code)
        top_nav = soup.find('section', id='top-bar')
        if 'My Bets' not in top_nav.text:
            return driver, my_bets_available

        # click my bets
        nav_my_bets_btn = driver.find_element_by_xpath('//*[@id="top-bar"]/div/nav[2]/div/a')
        nav_my_bets_btn.click()
        sleep(self.rest)

        # defaults to active if settled
        if settled:
            settled_btn = driver.find_element_by_xpath('//*[@id="main"]/div[2]/div/h1/div[1]/a[2]')
            settled_btn.click()
            sleep(self.rest)

        # check if user has sub accounts available
        try:
            sub_account = driver.find_element_by_id("account")
        except selenium.common.exceptions.NoSuchElementException:
            sub_account = False



